# Electrical Impedance Tomograpy

This repository holds code for my Bachelor of Engineeng thesis. Cosntructed device is designed to perform a measurement using Electrical Impedance Tomograpy technique to identify internal structure of a slice of some measured medium.

## Hardware

Most of the hardware was designed and assembled by me. Project also utilizes CY8CKIT-043 devboard with PSoC microcontroller. Below you can find a picture of a signal routing board and a block diagram of the whole system.

![](https://gitlab.com/JJ-ay/inzynierka_soft/-/raw/master/media/3.jpg)

![](https://gitlab.com/JJ-ay/inzynierka_soft/-/raw/master/media/2.png)

## Software

As it was my first experience with working on PSoC some things are probably messy. For code generation and writing I used *PSoC Creator* software. Below you can find an image depictiong used peripherial configuaration.

![](https://gitlab.com/JJ-ay/inzynierka_soft/-/raw/master/media/1.PNG)

## Image Reconstruction

For EIT reconstruction I utilized [EIDORS](https://eidors3d.sourceforge.net/) software in Matlab. In `matlab/` directory you can find a set of example data and a script that uses EIDORS library to reconstruct image from provided measurement arrays (I'm sorry but for the life of mine I can't remember what the arrays' names mean). Below you can find an image that's crowning my thesis. It shows a comparison between example measured object and its reconstructed image. Not a complete success (probably due to tarnished electrodes or faulty electric connection) but I was quite proud anyways.

![](https://gitlab.com/JJ-ay/inzynierka_soft/-/raw/master/media/4.png)
