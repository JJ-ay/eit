
n_elec= 16;
n_rings= 1;
options = {'no_meas_current','no_meas_current_next1','no_rotate_meas'};

[st, ms]= mk_stim_patterns(n_elec, n_rings, '{ad}','{ad}', options, 0.0002);
inv2d= mk_common_model('c2c',n_elec);
inv2d.fwd_model.stimulation = st;
inv2d.fwd_model.meas_select = ms;

zero = zeros(256,1);
data = table2array(avgone);

img= inv_solve( inv2d, data, zero);
img.calc_colours.cmap_type = 'greyscale';
show_fem(img);

