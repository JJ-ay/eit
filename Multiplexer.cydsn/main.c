
#include <project.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <expanders.h>

#define LED_ON 0
#define LED_OFF 1
#define DEFAULT_FREQUENCY           10000u
#define PEAK_DET_DISCH_US           3u
#define START_POINT                 1       // pierwsza para aktywna [1..16]

CY_ISR_PROTO(BUTTON_PRESS);
CY_ISR_PROTO(ADC_READ);
void PSOC_Init(void);
void UART_SendPcComm(void);
void TransmitMode_Start(void);
void TransmitMode_End(void);

volatile uint32 dFrequency = DEFAULT_FREQUENCY;
volatile uint8 bMeasure = 0;     // przerwanie od guzika zmienia na 1 i zaczyna pomiar
volatile uint8 dActivePair = START_POINT;
volatile uint16 ADC_BuffCH1;
volatile uint16 ADC_BuffCH2;
uint16 pdMeasurementBuff [16];
uint8 dTransferStatus = TRANSFER_CMPLT;  
uint8 dProgramStatus = 0;   // 0 == COMPLETE | 1 == ABORTED | 2 == ERROR
uint8 dIterator;

int main (void)
{  
    PSOC_Init();

    for(;;)
    {   
        LED_Write(LED_OFF);
        bMeasure = 0;
        dTransferStatus = TRANSFER_CMPLT;
        dProgramStatus = 0;

        while(!bMeasure)
        {
            // czytanie komend z uarta
            __ASM("nop");
        }

        LED_Write(LED_ON);

        // for(int i=0; i<16; i++)
        if((bMeasure) && (!dProgramStatus))
        {
            // ustawienie linii aktywnych
            if(dActivePair<16)
                dTransferStatus |= EXP_SetActive(dActivePair, dActivePair+1);
            else
                dTransferStatus |= EXP_SetActive(dActivePair, 1);
            if(dTransferStatus == TRANSFER_ERROR){dProgramStatus = 2;}            
            dIterator = (dActivePair<=13)?(dActivePair+3):(dActivePair-13);    
            
            for(int j=0; j<16; j++)
            {
                // pomiar na liniach pasywnych
                if((bMeasure) && (!dProgramStatus))
                {
                    if(dIterator<16)
                        dTransferStatus |= EXP_SetPassive(dIterator, dIterator+1);
                    else
                        dTransferStatus |= EXP_SetPassive(dIterator, 1);
                    if(dTransferStatus == TRANSFER_ERROR){dProgramStatus = 2;}    
                    
                    if((bMeasure) && (!dProgramStatus))
                    {
                        // meas
                    }
                }
                else
                {
                    EXP_ClearActive();
                    EXP_ClearPassive();
                    break;
                }
            }

            // UART PC_COMM   
        }
        else
        {
            EXP_ClearActive();
            EXP_ClearPassive();
            // break;
        }
        
        // UART final msg
    }
}

void PSOC_Init(void)
{
    CyGlobalIntEnable;      

    UART_Start();
    I2C_Start();
    ADC_EIT_Start();
//    ADC_EIT_IRQ_Disable();
//    ADC_EIT_StartConvert();
    
//    ADC_EIT_IRQ_StartEx(ADC_READ);
    ADC_EIT_EOC_INT_StartEx(ADC_READ);
    ADC_EIT_EOC_INT_Disable();
    
}

CY_ISR(BUTTON_PRESS)
{
    bMeasure = 1;
    //button clear int
}

CY_ISR(ADC_READ)
{
    ADC_BuffCH1 = ADC_EIT_GetResult16(0);
    ADC_BuffCH2 = ADC_EIT_GetResult16(1);
    ADC_EIT_StopConvert();
    ADC_EIT_IRQ_ClearPending();
}

void TransmitMode_Start(void)
{
    // Mode solely for uininterrupted UART transmission
    
    ADC_EIT_IRQ_Disable();
    //button int dis
}

void TransmitMode_End(void)
{
    ADC_EIT_IRQ_ClearPending();
    ADC_EIT_IRQ_Enable();
    //buttom int en
}