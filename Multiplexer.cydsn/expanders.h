
#include <project.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRANSFER_CMPLT      0x00u
#define TRANSFER_ERROR      0xFFu

#define EXP_ADDR_ACTIVE    0x21u
#define EXP_ADDR_PASSIVE   0x20u

#define EXP_REG_GP0        0x0u
#define EXP_REG_GP1        0x1u
#define EXP_REG_OLAT0      0x2u
#define EXP_REG_OLAT1      0x3u
#define EXP_REG_IPOL0      0x4u
#define EXP_REG_IPOL1      0x5u
#define EXP_REG_IODIR0     0x6u
#define EXP_REG_IODIR1     0x7u
#define EXP_REG_INTCAP0    0x8u
#define EXP_REG_INTCAP1    0x9u
#define EXP_REG_IOCON0     0xAu
#define EXP_REG_IOCON1     0xBu

#define EXP_REG_IO_ODD	EXP_REG_GP0
#define EXP_REG_IO_EVN	EXP_REG_GP1  

uint8 EXP_Electrodes[16] =  {
    0x01u,         
    0x01u,         
    0x02u,         
    0x02u,         
    0x04u,         
    0x04u,         
    0x08u,         
    0x08u,         
    0x10u,         
    0x10u,         
    0x20u,         
    0x20u,         
    0x40u,         
    0x40u,
    0x80u,         
    0x80u
};

typedef enum datadir{
    Read,
    Write
} DataDir_t;

uint32 I2C_Write(uint8 slave_REG, DataDir_t read_or_write, uint8 cmd, uint8 value);
uint32 I2C_Read(uint8 slave_REG, uint8 * read_buff); 
uint32 EXP_SetPassive(uint8_t electrode1, uint8_t electrode2);
uint32 EXP_SetActive(uint8_t electrode1, uint8_t electrode2);
uint32 EXP_ClearActive(void);
uint32 EXP_ClearPassive(void);

/* [] END OF FILE */
