/*******************************************************************************
* File Name: ADC_EIT_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_EIT.h"


/***************************************
* Local data allocation
***************************************/

static ADC_EIT_BACKUP_STRUCT  ADC_EIT_backup =
{
    ADC_EIT_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: ADC_EIT_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_EIT_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_EIT_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_EIT_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_EIT_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_EIT_backup - modified.
*
*******************************************************************************/
void ADC_EIT_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    ADC_EIT_backup.dftRegVal = ADC_EIT_SAR_DFT_CTRL_REG & (uint32)~ADC_EIT_ADFT_OVERRIDE;
    ADC_EIT_SAR_DFT_CTRL_REG |= ADC_EIT_ADFT_OVERRIDE;
    if((ADC_EIT_SAR_CTRL_REG  & ADC_EIT_ENABLE) != 0u)
    {
        if((ADC_EIT_SAR_SAMPLE_CTRL_REG & ADC_EIT_CONTINUOUS_EN) != 0u)
        {
            ADC_EIT_backup.enableState = ADC_EIT_ENABLED | ADC_EIT_STARTED;
        }
        else
        {
            ADC_EIT_backup.enableState = ADC_EIT_ENABLED;
        }
        ADC_EIT_StopConvert();
        ADC_EIT_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((ADC_EIT_SAR_CTRL_REG & ADC_EIT_BOOSTPUMP_EN) != 0u)
        {
            ADC_EIT_SAR_CTRL_REG &= (uint32)~ADC_EIT_BOOSTPUMP_EN;
            ADC_EIT_backup.enableState |= ADC_EIT_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        ADC_EIT_backup.enableState = ADC_EIT_DISABLED;
    }
}


/*******************************************************************************
* Function Name: ADC_EIT_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_EIT_backup - used.
*
*******************************************************************************/
void ADC_EIT_Wakeup(void)
{
    ADC_EIT_SAR_DFT_CTRL_REG = ADC_EIT_backup.dftRegVal;
    if(ADC_EIT_backup.enableState != ADC_EIT_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((ADC_EIT_backup.enableState & ADC_EIT_BOOSTPUMP_ENABLED) != 0u)
        {
            ADC_EIT_SAR_CTRL_REG |= ADC_EIT_BOOSTPUMP_EN;
        }
        ADC_EIT_Enable();
        if((ADC_EIT_backup.enableState & ADC_EIT_STARTED) != 0u)
        {
            ADC_EIT_StartConvert();
        }
    }
}
/* [] END OF FILE */
