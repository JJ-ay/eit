
#include <expanders.h>

uint32 I2C_Write(uint8 slave_addr, DataDir_t read_or_write, uint8 cmd, uint8 value)
{
    uint8  buffer[2];
    uint32 status = TRANSFER_ERROR;

    /* Initialize buffer with packet */
    buffer[0] = cmd;
    buffer[1] = value;
    I2C_I2CMasterClearStatus();
    
    switch(read_or_write)
    {
        case Read:
             if (I2C_I2C_MSTR_NO_ERROR == I2C_I2CMasterWriteBuf(slave_addr,buffer,1,I2C_I2C_MODE_COMPLETE_XFER))
            {
                while (0u == (I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT))
                {
                    /* Wait */
                }
                if (0u == (I2C_I2CMasterStatus() & I2C_I2C_MSTAT_ERR_XFER))
                {
                    /* Check if all bytes were written */
                    if (I2C_I2CMasterGetWriteBufSize() == 1)
                    {
                        status = TRANSFER_CMPLT;
                    }
                }
            }
        break;
        case Write:
            if (I2C_I2C_MSTR_NO_ERROR == I2C_I2CMasterWriteBuf(slave_addr,buffer,2,I2C_I2C_MODE_COMPLETE_XFER))
            {
                while (0u == (I2C_I2CMasterStatus() & I2C_I2C_MSTAT_WR_CMPLT))
                {
                    /* Wait */
                }
                if (0u == (I2C_I2CMasterStatus() & I2C_I2C_MSTAT_ERR_XFER))
                {
                    /* Check if all bytes were written */
                    if (I2C_I2CMasterGetWriteBufSize() == 2)
                    {
                        status = TRANSFER_CMPLT;
                    }
                }
            }
        break;
    }
    return status;
}

uint32 I2C_Read(uint8 slave_addr, uint8 * read_buff)
{
    uint32 status = TRANSFER_ERROR;
    
    (void) I2C_I2CMasterClearStatus();
    
    if(I2C_I2C_MSTR_NO_ERROR ==  I2C_I2CMasterReadBuf(slave_addr, read_buff, 2, I2C_I2C_MODE_COMPLETE_XFER))
    {
        while (0u == (I2C_I2CMasterStatus() & I2C_I2C_MSTAT_RD_CMPLT))
        {
            /* Wait */
        }
        if (0u == (I2C_I2C_MSTAT_ERR_XFER & I2C_I2CMasterStatus()))
        {
            /* Check packet structure */
            if (I2C_I2CMasterGetReadBufSize() == 2)
            {
                    status = TRANSFER_CMPLT;
            }
        }
    }

    return (status);  
}

uint32 EXP_SetPassive(uint8_t electrode1, uint8_t electrode2)
{
    uint32 status = TRANSFER_CMPLT;
    uint8 e1_reg = (electrode1%2)?(EXP_REG_IO_ODD):(EXP_REG_IO_EVN);
    uint8 e1_val = EXP_Electrodes[electrode1-1];
    uint8 e2_reg = (electrode2%2)?(EXP_REG_IO_ODD):(EXP_REG_IO_EVN);;
    uint8 e2_val = EXP_Electrodes[electrode2-1];
    
    status |= I2C_Write(EXP_ADDR_PASSIVE,Write,e1_reg,e1_val);
    status |= I2C_Write(EXP_ADDR_PASSIVE,Write,e2_reg,e2_val);
    
    return status;
}

uint32 EXP_SetActive(uint8_t electrode1, uint8_t electrode2)
{
    uint32 status = TRANSFER_CMPLT;
    uint8 e1_reg = (electrode1%2)?(EXP_REG_IO_ODD):(EXP_REG_IO_EVN);
    uint8 e1_val = EXP_Electrodes[electrode1-1];
    uint8 e2_reg = (electrode2%2)?(EXP_REG_IO_ODD):(EXP_REG_IO_EVN);;
    uint8 e2_val = EXP_Electrodes[electrode2-1];
    
    status |= I2C_Write(EXP_ADDR_ACTIVE,Write,e1_reg,e1_val);
    status |= I2C_Write(EXP_ADDR_ACTIVE,Write,e2_reg,e2_val);
    
    return status;
}

uint32 EXP_ClearActive(void)
{
    uint32 status = TRANSFER_CMPLT;
    status |= I2C_Write(EXP_ADDR_ACTIVE,EXP_REG_GP0,Write,0x00);
    status |= I2C_Write(EXP_ADDR_ACTIVE,EXP_REG_GP1,Write,0x00);
    return status;
}

uint32 EXP_ClearPassive(void)
{
    uint32 status = TRANSFER_CMPLT;
    status |= I2C_Write(EXP_ADDR_PASSIVE,EXP_REG_GP0,Write,0x00);
    status |= I2C_Write(EXP_ADDR_PASSIVE,EXP_REG_GP1,Write,0x00);
    return status;
}

/* [] END OF FILE */
